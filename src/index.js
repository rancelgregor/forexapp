import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
//this is our Counter app
//import App from "./App";

//This is our Forex App
// import ForexApp from "./ForexApp";
// import CalculatorApp from "./CalculatorApp";
import Calc from "./Calc/Calc";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.css";

ReactDOM.render(
  <React.StrictMode>
    <Calc />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
