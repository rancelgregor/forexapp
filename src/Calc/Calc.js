import React, { useState } from "react";
import Button from "./Button";

const Calc = () => {
  /*States */
  const [input, setInput] = useState("");
  const [previousNumber, setPreviousNumber] = useState("");
  let [currentNumber, setCurrentNumber] = useState("");
  const [operator, setOperator] = useState("");
  const [operatorText, setOperatorText] = useState("");

  /*Functionalities */
  const addToInput = val => {
    setInput(input + val);
  };

  const addToDecimal = val => {
    if (input.indexOf(".") === -1) {
      setInput(input + val);
    }
  };

  const clear = () => {
    setInput("");
    setPreviousNumber("");
    setOperatorText("");
  };

  const addZero = val => {
    if (input != "") {
      setInput(input + val);
    }
  };

  /*Operations */

  const add = val => {
    setPreviousNumber(input);
    setInput("");
    setOperator("plus");
    setOperatorText(val);
  };

  const divide = val => {
    setPreviousNumber(input);
    setInput("");
    setOperator("divide");
    setOperatorText(val);
  };

  const multiply = val => {
    setPreviousNumber(input);
    setInput("");
    setOperator("multiply");
    setOperatorText(val);
  };

  const subtract = val => {
    setPreviousNumber(input);
    setInput("");
    setOperator("subtract");
    setOperatorText(val);
  };

  /*Evaluation */
  const equals = () => {
    currentNumber = input;
    if (operator === "add") {
      let result = parseFloat(currentNumber) + parseFloat(previousNumber);
      let res = result.toString();
      setInput(res);
      setOperatorText("");
      setPreviousNumber("");
    } else if (operator === "minus") {
      let result = parseFloat(currentNumber) - parseFloat(previousNumber);
      let res = result.toString();
      setInput(res);
      setOperatorText("");
      setPreviousNumber("");
    } else if (operator === "multiply") {
      let result = parseFloat(currentNumber) * parseFloat(previousNumber);
      let res = result.toString();
      setInput(res);
      setOperatorText("");
      setPreviousNumber("");
    } else if (operator === "divide") {
      let result = parseFloat(currentNumber) / parseFloat(previousNumber);
      let res = result.toString();
      setInput(res);
      setOperatorText("");
      setPreviousNumber("");
    }
  };
  return (
    <React.Fragment>
      <div className="d-flex flex-column align-items-center justify-content-center my-5">
        <div className="row">
          <div
            className="col-lg-12 border border-dark border-thick bg-primary"
            style={{ borderWidth: "thick" }}
          >
            <div className="row bg-light">
              <span className="text-dark p-3 h1">
                {previousNumber === "" ? "" : previousNumber}{" "}
                {operatorText === "" ? "" : operatorText}{" "}
                {input === "" ? "0" : input}
              </span>
            </div>
            <div className="row">
              <Button text={"7"} color={"gray"} onClick={addToInput} />
              <Button text={"8"} color={"gray"} onClick={addToInput} />
              <Button text={"9"} color={"gray"} onClick={addToInput} />
              <Button text={"/"} color={"green"} onClick={divide} />
            </div>
            <div className="row">
              <Button text={"4"} color={"gray"} onClick={addToInput} />
              <Button text={"5"} color={"gray"} onClick={addToInput} />
              <Button text={"6"} color={"gray"} onClick={addToInput} />
              <Button text={"X"} color={"green"} onClick={multiply} />
            </div>
            <div className="row">
              <Button text={"1"} color={"gray"} onClick={addToInput} />
              <Button text={"2"} color={"gray"} onClick={addToInput} />
              <Button text={"3"} color={"gray"} onClick={addToInput} />
              <Button text={"+"} color={"green"} onClick={add} />
            </div>
            <div className="row">
              <Button text={"."} color={"gray"} onClick={addToDecimal} />
              <Button text={"0"} color={"gray"} onClick={addZero} />
              <Button text={"="} color={"gray"} onClick={equals} />
              <Button text={"-"} color={"green"} onClick={subtract} />
            </div>
            <div className="d-flex justify-content-center">
              <button className="btn btn-danger w-100 my-3" onClick={clear}>
                Clear
              </button>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default Calc;
