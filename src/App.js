import React, { Component } from "react";
import Box from "./components/Box";
import Buttons from "./components/Buttons";

class App extends Component {
  state = {
    count: 0
  };

  //Step1: Create the function
  handleAdd = () => {
    //Step5: Set up the setState function based on role of button
    this.setState({
      count: this.state.count + 1
    });
  };

  handleMinus = () => {
    this.setState({
      count: this.state.count - 1
    });
  };

  handleReset = () => {
    this.setState({
      count: this.state.count * 0
    });
  };

  handleMultiply2 = () => {
    this.setState({
      count: this.state.count * 2
    });
  };

  render() {
    return (
      <React.Fragment>
        <Box count={this.state.count} />
        <Buttons
          //Step2:Pass the function as a property to Buttons Component
          handleAdd={this.handleAdd}
          handleMinus={this.handleMinus}
          handleReset={this.handleReset}
          handleMultiply2={this.handleMultiply2}
        />
      </React.Fragment>
    );
  }
}

export default App;
