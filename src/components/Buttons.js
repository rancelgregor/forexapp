import React, { Component } from "react";
import Button from "./Button";
class Buttons extends Component {
  render() {
    return (
      <div
        className="d-flex justify-content-around"
        style={{
          margin: "0 200px"
        }}
      >
        <Button
          color={"btn btn-info"}
          text={"+1"}
          //Step3: Pass the function we received as a property to Button Component
          handleOnClick={this.props.handleAdd}
        />
        <Button
          color={"btn btn-warning"}
          text={"-1"}
          handleOnClick={this.props.handleMinus}
        />

        <Button
          color={"btn btn-danger"}
          text={"+Reset"}
          handleOnClick={this.props.handleReset}
        />
        <Button
          color={"btn btn-primary"}
          text={"x2"}
          handleOnClick={this.props.handleMultiply2}
        />
      </div>
    );
  }
}

export default Buttons;
