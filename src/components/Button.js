import React, { Component } from "react";

class Button extends Component {
  render() {
    return (
      <button
        className={this.props.color}
        //Step4: Use the function we received as property in our event listener property(onClick)
        onClick={this.props.handleOnClick}
      >
        {this.props.text}
      </button>
    );
  }
}
export default Button;
