import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

function ForexInput(props) {
  return (
    <FormGroup className="d-flex flex-column align-items-center">
      <Label>{props.label}</Label>
      <Input
        placeholder={props.placeholder}
        defaultValue={props.defaultValue}
        onChange={props.onChange}
        type="number"
      />
    </FormGroup>
  );
}

export default ForexInput;
