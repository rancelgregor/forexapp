import React from "react";
import { Table } from "reactstrap";

const ForexTable = props => {
  return (
    <Table className="striped border">
      <Thead>
        <tr>
          <th>Currency</th>
          <th>Rate</th>
        </tr>
      </Thead>
      <tbody></tbody>
    </Table>
  );
};

export default ForexTable;
