const currencies = [
  {
    currency: "Philippine Peso",
    code: "PHP"
  },
  {
    currency: "US Dollars",
    code: "USD"
  },
  {
    currency: "Euro",
    code: "EUR"
  },
  {
    currency: "Thai Baht",
    code: "THB"
  },
  {
    currency: "South Korean Won",
    code: "KRW"
  },
  {
    currency: "British  Pound",
    code: "GBP"
  },
  {
    currency: "Japanese Yen",
    code: "JPY"
  },
  {
    currency: "Indonesian Ringgit",
    code: "IDR"
  }
];

export default currencies;
