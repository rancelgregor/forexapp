import React, { useState } from "react";
import {
  Form,
  FormGroup,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Label,
  DropdownItem
} from "reactstrap";
import Currencies from "./forexData";

function ForexDropdown(props) {
  const [dropdownIsOpen, setDropdownIsOpen] = useState(false);
  const [currency, setCurrency] = useState(null);

  return (
    <FormGroup className="d-flex flex-column align-items-center">
      <Label>{props.label}</Label>
      <Dropdown
        isOpen={dropdownIsOpen}
        toggle={() => setDropdownIsOpen(!dropdownIsOpen)}
      >
        <DropdownToggle caret>
          {!props.currency ? "Choose Currency" : props.currency.currency}
        </DropdownToggle>
        <DropdownMenu>
          {Currencies.map((currency, index) => (
            <DropdownItem key={index} onClick={() => props.onClick(currency)}>
              {currency.currency}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    </FormGroup>
  );
}

export default ForexDropdown;
