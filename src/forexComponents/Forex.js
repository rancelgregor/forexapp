import React, { useState } from "react";
import ForexDropdown from "./ForexDropdown";
import ForexInput from "./ForexInput";
import { Button } from "reactstrap";
import ForexTable from "./ForexTable";

const Forex = () => {
  const [amount, setAmount] = useState(0);
  const [baseCurrency, setBaseCurrency] = useState("");
  const [targetCurrency, setTargetCurrency] = useState("");
  const [convertedAmount, setConvertedAmount] = useState(0);

  const handleAmount = event => {
    setAmount(event.target.value);
  };

  const handleBaseCurrency = currency => {
    if (!currency) {
      alert("Base Currency Required!");
    } else {
      setBaseCurrency(currency);
    }
  };

  const handleTargetCurrency = currency => {
    setTargetCurrency(currency);
  };

  const handleConvert = () => {
    if (targetCurrency === "" && baseCurrency === "" && amount <= 0) {
      alert("Nothing Selected");
    } else if (targetCurrency === "") {
      alert("No Target Currency Selected");
    } else if (baseCurrency === "") {
      alert("No Base Currency Selected");
    } else if (amount <= 0) {
      alert("Input must be greater than zero");
    } else {
      const code = baseCurrency.code;
      console.log("https://api.exchangeratesapi.io/latest?base=" + code);
      fetch("https://api.exchangeratesapi.io/latest?base=" + code)
        .then(res => res.json())
        .then(res => {
          // console.log(res);
          const targetCode = targetCurrency.code;
          const rate = res.rates[targetCode];
          const convertedAmount = amount * rate;
          if (convertedAmount < 0) {
            alert("Invalid Amount");
          } else {
            setConvertedAmount(amount * rate);
            console.log(targetCurrency.code);
          }
        });
    }
  };

  console.log(amount);
  return (
    <div>
      <h1 className="text-center my-5">Forex Calculator</h1>
      <div
        className="d-flex justify-content-around border border-warning"
        style={{ margin: "0 20px", width: "50vw" }}
      >
        <ForexDropdown
          label={"Base Currency"}
          onClick={handleBaseCurrency}
          currency={baseCurrency}
        />
        <ForexDropdown
          label={"Target Currency"}
          onClick={handleTargetCurrency}
          currency={targetCurrency}
        />
      </div>
      <div className="d-flex justify-content-around my-5">
        <ForexInput
          label={"Amount"}
          placeholder={"Amount to convert"}
          onChange={handleAmount}
        />
        <Button color="info" onClick={handleConvert}>
          Convert
        </Button>
      </div>
      <div>
        <h1 className="text-center my-5">
          {convertedAmount}
          {targetCurrency.code}
        </h1>
      </div>
    </div>
  );
};

export default Forex;
